
// Start of Activity at Line 136

//Basic Server with Mongoose

const express = require("express");
// Mongoose is a package that allows creation of Schemas to model our data structures
// It also has access to a number of methods for manipulating our database
const mongoose = require("mongoose");
const app = express();
const port = 3001;

mongoose.connect( 
	"mongodb+srv://admin:admin@zuitt-bootcamp.z9use.mongodb.net/batch127_to-do?retryWrites=true&w=majority",
		{
			useNewUrlParser: true, 	// to avoid any current or future errors
			useUnifiedTopology: true // to prevent current or futureerrors
	});
// Connect to the database by passing in your connection string
// Remember to replace <password> and database name with actual values
// Para maiconnect ang mongodb atlas sa server 


// Connecting to Robo3T locally,
/*
mongoose.connect("mongodb://localhost:27017//databaseName",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
})
*/

let db = mongoose.connection;
// Set notifications for connection success or failure
// Connection to the database, allows us to handle error when initial connection is established

db.on("error", console.error.bind(console, "connection error"));
// If a connection error occured, output in the console
	// console.error.bind(console) = allows us to print error in the browser console and in the terminal

db.once("open", () => console.log("We're connected to the cloud database"))
// If the connection is successful, output in the console

app.use(express.json())
app.use(express.urlencoded({ extended:true }))

// Mongoose Schema
// Schemas determine the structure of the documents to be written in the database
// Schema act as blueprints of our data
// Use the Schema() constructor of the Mongoose module to create a new Schema object

const taskSchema = new mongoose.Schema({
	//Define the fields with the corresponding data type
	// for task, it needs a "task name" and "task status"
	name: String,
	status: {
		type: String,
		// Default valuse are the pre-defined values for a field if we dont put any value
		default: "pending"
	}
})
const Task = mongoose.model("Task", taskSchema) 
// "Task" param of the mongoose model method indicates the collection in where to store the data
// taskSchema param is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection
// .model method ang bahalang magdala ng data sa database natin

// Task is capitalized following the MVC approach for naming conventions
	// Mode-View-Controller
// dito nilalagay ang schema. Acts as middleman ng server natin
// Every schema, may ganito dapat na variable

// The "new" keyword create a new schema

// CREATE A NEW TASK
/*
Business Logic
1. Add a funstionality to check if there are duplicate tasks
	- if the task already exists in the database, we return an error
	- if the task doesnt exist in the database,we will add it in the database
2. The task data will be coming from the request's body
3. Create a new Task object with a "name" field/property
4. The "status" property does not need to be provided because our schema default is to "pending" upon creation of an object
*/
app.post("/tasks", (req, res)=>{
	// check if there are duplicate tasks
	Task.findOne( {name: req.body.name}, (err, result)=>{
		// findOne()) is a mongoose method that acts similar to "find" of MongoDB
			// it returns the first document that matches the search criteria
		// if a document was found and the document name matches the information sent via the client/postman
		if(result !== null && result.name == req.body.name){
			// return a message to the client
			return res.send("Duplicate task found")
		}else{
			//  if no document was found, create a new task and save it to our database
			let newTask = new Task({
				name: req.body.name
			})

			newTask.save((saveErr, saveTask)=>{
				if(saveErr){
					return console.log(saveErr)
				}else{
					return res.status(201).send("New task created")
				}
			})
			//.save automatic issave sa database
		}
	})

})

// Getting aAll the tasks
/*

Business Logic
1. Retrieve all the documents usign the get method
2. If error is encountered, print the error
3. If no errors are found, send a success status back to the client/postman and return an array of document
*/
app.get("/tasks", (req, res)=>{
	// "find" is a mongoose method that is similar to MongoDB "find"
	Task.find({}, (err, result)=>{
		// error handling
		if(err){
			return console.log(err)
		}else{
			// if no errors are found
			// status 200 means that everything is OK in terms of processing
			return res.status(200).json({
				// the "json" method allows to send a JSON format for the response
				data: result
			})
				// the returned response is purposefully returned as an objects with the "data" property tomirror real world complex data structures.
		}
	})
	// empty string {} means it returns ALL the documents and stores them in one result parameter of the callback function
})

// Start of Activity

/*
Business Logic for POST /signup
1. Add a functionality if there are duplicate tasks
	= if the user already exists in the database, we return an error
	= if the user does not exist in the database, we add it in the databse
2. The user data will be coming from the request's body
3. Create anew user object with a "username" and "password" field/postman
*/

// Create a User schema
const userSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
})

// Create a User model.
const User = mongoose.model("User", userSchema)

// Create a POST route that will access the "/signup" route that will create a user.
// Process a POST request at the "/signup" route using postman to register a user.
app.post("/signup", (req, res)=>{
	User.findOne( {name: req.body.name}, (err, result)=>{
		if(result !== null && result.name == req.body.name){
			return res.send("Duplicate task found")
		}else{
			let newUser = new User({
				name: req.body.name
			})

			newUser.save((saveErr, saveUser)=>{
				if(saveErr){
					return console.log(saveErr)
				}else{
					return res.status(201).send("New task created")
				}
			})
		}
	})
})

/*Business Logic for Getting ALL the tasks
1. Retrieve all the documents usign the get method
2. If error is encountered, print the error
3. If no errors are found, send a success status back to the client/postman and return an array of document*/ 

// Create a GET route that will return all users
app.get("/users", (req, res)=>{
	User.find({}, (err, result)=>{
		if(err){
			return console.log(err)
		}else{
			return res.status(200).json({
				userData: result
			})
		}
	})
})

app.listen(port, () => console.log(`Server is running at ${port}`))
